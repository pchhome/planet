﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainUI : MonoBehaviour {
	public GameObject help;
	public Text playerName;
	public GameObject restartButton;
	public GameObject rankButton;
	public GameObject arrow;
	public Text playTime;
	int playTimeNum;
	public Text leftNum;
	public Transform map;
	public GameObject[] planet;
	Vector3[] move;
	// float force = 
	float[] gravity;
	List<GameObject>[] shadow;
	int[] shadowMax;
	int selectId = 1;
	int showId = 0;
	float GetAngle(Vector3 to)
	{
		return GetAngle2(new Vector3(0, 1, 0), to);
	}
	float GetAngle2(Vector3 from_, Vector3 to_){
		Vector3 v3 = Vector3.Cross(from_,to_);
		if(v3.z > 0)
			return Vector3.Angle(from_,to_);
		else
			return 360-Vector3.Angle(from_,to_);
	}
	void Awake()
	{
        Application.targetFrameRate = 60;
#if UNITY_EDITOR
        Application.targetFrameRate = 30;
#endif

		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		Global.InitUI();
	}
	void Start()
	{
		// Global.SetBest(6, 333);
		// Global.SetBest(7, 443);
		// Global.SetBest(8, 553);
		// Global.testId = "6";

		if(Global.GetGameTime() == 0)
		{
			help.SetActive(true);
			LeanTween.scale(help, Vector3.one, 1f).setOnComplete(()=>{
				LeanTween.moveLocalY(help, -222, 0.5f);
				LeanTween.delayedCall(1f, ()=>{
					help.SetActive(false);
				});
			});
		}

		move = new Vector3[planet.Length];
		gravity = new float[planet.Length];
		shadow = new List<GameObject>[planet.Length];
		shadowMax = new int[planet.Length];
		// move[0] = new Vector3(-1, -1, 0);

		for(int i = 0; i < planet.Length; ++i)
		{
			gravity[i] = 1;
			shadow[i] = new List<GameObject>();
			shadowMax[i] = 0;

			for(int i2 = 0; i2 < planet.Length; ++i2)
			{
				if(i != i2 && planet[i].transform.localScale.x == planet[i2].transform.localScale.x)
				{
					Debug.LogError(i + " " + i2 + " " + planet[i].transform.localScale.x);
					break;
				}
			}
		}

		playTimeNum = 0;
		InvokeRepeating("ShowNum", 0, 1);
	}
	void ShowNum()
	{
		int gameTime = Global.GetGameTime();
		Global.SetGameTime(++gameTime);

		int n = 0;
		for(int i = 1; i < planet.Length; ++i)
		{
			if(showId >= i && planet[i].activeSelf)
			{
				++n;
			}
		}
	
		int n2 = 8 - showId;
		leftNum.text = n2.ToString();
		if(n2 < 1)
		{
			leftNum.text = "";
		}
			
		++playTimeNum;
		playTime.text = "";

		// if(n < 6 || n2 != 0)
		if(n2 != 0)
		{
			// playTime.text = n.ToString();
			// if(n < 1)
			// {
			// 	playTime.text = "";
			// }
			return;
		}
		Global.SaveData(n, playTimeNum);
		
		int best = Global.GetBest(n);
		int start = Global.GetStart(n);
		int n5 = playTimeNum - start;
		
		playTime.text = n + "\nBest " + Global.GetTime(best) + "\n" + Global.GetTime(n5);
	}
	bool isDragging = false;
	Vector3 dragPosition;
	int shadowTime = 0;
	void Update()
	{
		++shadowTime;

		if(shadowTime > 5)
		{
			shadowTime = 0;
		}

		for(int i = 0; i <= showId; ++i)
		{
			if(!planet[i].activeSelf)
			{
				continue;
			}
			
			planet[i].transform.Rotate(Vector3.forward * -2);

			if(i == 0)
			{
				continue;
			}

			Vector3 v = planet[i].transform.position;
			float f = v.x * v.x + v.y * v.y;
			Vector3 v2 = Vector3.Normalize(v);
			move[i] -= v2 * gravity[i] / f;
			// print(i);
			// planet[i].transform.position = v + move[i] * Time.deltaTime * 5f;
			planet[i].transform.position = v + move[i] * 5 / 30f;

			if(shadowTime == 0)
			{
				GameObject p = Instantiate(planet[i], map);
				Vector3 v3 = planet[i].transform.localScale * 0.2f;
				p.transform.localScale = Vector3.zero;
				shadow[i].Add(p);

				LeanTween.scale(p, v3, 1);

				int n = shadow[i].Count;
				if(n > 11)
				{
					for(int i2 = 0; i2 < 9; ++i2)
					{
						if(Mathf.Abs(Vector3.Distance(v, shadow[i][0].transform.position)) < 2)
						{
							Destroy(shadow[i][0]);
							shadow[i].RemoveAt(0);
						}
					}
				}
				
				if(shadow[i].Count > 222)
				{
					Destroy(shadow[i][0]);
					shadow[i].RemoveAt(0);
				}

				p.transform.position = v;

				if(Mathf.Abs(v.x) > 33 || Mathf.Abs(v.y) > 33)
				{
					HidePlanet(i);
				}
			}
			
			for(int i2 = 0; i2 < planet.Length; ++i2)
			{
				if(i != i2 && planet[i].activeSelf && planet[i2].activeSelf &&
					Vector3.Distance(planet[i].transform.position, planet[i2].transform.position) < 1)
				{
					if(planet[i].transform.localScale.x < planet[i2].transform.localScale.x)
					{
						HidePlanet(i);
					}
					else
					{
						HidePlanet(i2);
					}
				}
			}
		}

		if(selectId >= planet.Length)
		{
			return;
		}
		if(CheckGuiRaycastObjects())
		{
			return;
		}
		if (Input.GetMouseButtonDown(0))
        {
			if(!isDragging)
			{
				// print("d" + selectId);
				isDragging = true;
				dragPosition = Input.mousePosition;
				Vector3 worldPos = Camera.main.ScreenToWorldPoint(dragPosition);
				worldPos.z = 0;
				planet[selectId].transform.localPosition = worldPos;
				planet[selectId].SetActive(true);
			}
        }
		if (isDragging)
		{
			float x = Input.mousePosition.x - dragPosition.x;
			float y = Input.mousePosition.y - dragPosition.y;
			move[selectId] = new Vector3(x, y, 0) * 0.003f;
			// print(x + " " + y);

			Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 v2 = planet[selectId].transform.localPosition;
			worldPos.z = 0;
			v2 = new Vector3((v2.x + worldPos.x) / 2, (v2.y + worldPos.y) / 2, 0);
			arrow.transform.localPosition = v2;
			arrow.SetActive(true);
			arrow.transform.rotation = Quaternion.Euler(Vector3.forward * GetAngle(new Vector3(x, y, 0)));
			Vector3 v = arrow.transform.localScale;
			v.y = (x * x + y * y) * 0.00003f;
			arrow.transform.localScale = v;
		}
		if (Input.GetMouseButtonUp(0))
        {
			// print("up");
			arrow.SetActive(false);
			isDragging = false;
			showId = selectId;
			++selectId;
		}
	}
	bool CheckGuiRaycastObjects()//测试UI射线
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);

        eventData.pressPosition = Input.mousePosition;

        eventData.position = Input.mousePosition;

        List<RaycastResult> list = new List<RaycastResult>();

        GraphicRaycaster RaycastInCanvas = GameObject.Find("Canvas").GetComponent<GraphicRaycaster>();
        RaycastInCanvas.Raycast(eventData, list);

        //Debug.Log(list.Count);
        for (int i = 0; i < list.Count; ++i)
        {
            if (list[i].gameObject.GetComponent<Button>() != null)
            {
                return true;
            }
        }

        return false;
    }
	bool waitClick = false;
	void ShowClick(GameObject button)
	{
		waitClick = true;
		Vector3 v = button.transform.localScale;
		LeanTween.scale(button, Vector3.zero, 0.2f);
		LeanTween.delayedCall(0.4f, ()=>{
			LeanTween.scale(button, v, 0.2f);
		});
		LeanTween.delayedCall(0.6f, ()=>{
			waitClick = false;
		});
	}
	public void ClickRestart()
	{
		if(waitClick)
		{
			return;
		}
		ShowClick(restartButton);

		for(int i = 1; i < planet.Length; ++i)
		{
			HidePlanet(i);
		}

		selectId = 1;
		showId = 0;
		isDragging = false;

		Global.InitData();
	}
	void HidePlanet(int i)
	{
		planet[i].SetActive(false);

		for(int i2 = 0; i2 < shadow[i].Count; ++i2)
		{
			Destroy(shadow[i][i2]);
		}
		shadow[i].Clear();
		shadowMax[i] = 0;
	}
}
