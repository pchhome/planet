﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class Global : MonoBehaviour 
{
    // UI
    public static List<GameObject> uiList = new List<GameObject>();
    // public static GameObject startUI;
    public static MainUI mainUI;
	//public static RankUI rankUI;
    static GameObject AddUI(GameObject t, string s)
    {
        GameObject g = t.transform.Find(s).gameObject;
        uiList.Add(g);
        return g;
    }
    public static void InitUI()
    {
        uiList.Clear();

        GameObject c = GameObject.Find("Canvas");
        mainUI = AddUI(c, "MainUI").GetComponent<MainUI>();
        //rankUI = AddUI(c, "RankUI").GetComponent<RankUI>();
	}
	public static string GetTime(int n)
	{
        int n3 = n / 60;
        int n4 = n % 60;
		
		return GetInt2(n3) + ":" + GetInt2(n4);
	}
	public static string GetInt2(int n)
	{
		string s = n.ToString();
		if(s.Length < 2)
		{
			s = "0" + s;
		}

		return s;
	}
	static int pastNum = -1;
	public static void InitData()
	{
		pastNum = -1;
	}
	public static void SaveData(int num, int time)
	{
		if(num < 0)
		{
			return;
		}

		if(num != pastNum)
		{
			// save
			if(pastNum < 0)
			{
			}
			else
			{
				int best = GetBest(pastNum);
				int start = GetStart(pastNum);
				if(time - start > best)
				{
					best = time - start;
				}

				SetBest(pastNum, best);
			}

			SetStart(num, time);
			pastNum = num;
		}
		else
		{
			int best = GetBest(num);
			int start = GetStart(num);
			int n = time - start;
			if(n > best)
			{
				SetBest(num, n);
			}
		}
	}
	public static int GetBest(int num)
	{
		int n = PlayerPrefs.GetInt("best" + num, 0);
		// print("get " + n);
		return n;
	}
	public static void SetBest(int num, int n)
	{
		// print("set " + n);
		PlayerPrefs.SetInt("best" + num, n);
	}
	public static int GetStart(int num)
	{
		return PlayerPrefs.GetInt("start" + num, 0);
	}
	public static void SetStart(int num, int n)
	{
		PlayerPrefs.SetInt("start" + num, n);
	}
     
	public static void SetName(string s)
	{
		PlayerPrefs.SetString("name", s);
	}
	public static string GetName()
	{
		return PlayerPrefs.GetString("name", "");
	}
	public static void SetGameTime(int s)
	{
		PlayerPrefs.SetInt("gameTime", s);
	}
	public static int GetGameTime()
	{
		return PlayerPrefs.GetInt("gameTime", 0);
	}
	public static string GetMD5(string txt)
	{
		using (MD5 mi = MD5.Create())
		{
			byte[] buffer = Encoding.Default.GetBytes(txt);
			byte[] newBuffer = mi.ComputeHash(buffer);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < newBuffer.Length; i++)
			{
				sb.Append(newBuffer[i].ToString("x2"));
			}
			return sb.ToString();
		}
	}
	
}
