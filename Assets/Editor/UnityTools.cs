﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
using System.Diagnostics;
using System.IO;
using System;
using System.Linq;

[InitializeOnLoad]
public class UnityTools : MonoBehaviour {

    static void Log(string s)
    {
        UnityEngine.Debug.Log(s);
    }

    [MenuItem("Tools/Screenshot")]
    static void Screenshot()
    {

        DateTime dt = DateTime.Now;
        string s = dt.Hour + "_" + dt.Minute + "_" + dt.Second;

        ScreenCapture.CaptureScreenshot("Screen_" + s + ".png");

        Log("Screenshot");
    }
}
